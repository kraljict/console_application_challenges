﻿using _01_CafeRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CafeUI
{
    public class UI
    {
        private readonly MenuRepository _repo = new MenuRepository();

        public void Run()
        {
            SeedContent();
            RunMenu();
        }

        public void RunMenu()
        {
            bool continueToRun = true;

            while (continueToRun)
            {

                string Title = "The Komodo Kafe\n";
                Console.SetCursorPosition((Console.WindowWidth - Title.Length) / 2, Console.CursorTop);
                Console.WriteLine(Title);

                Console.WriteLine("What would you like to do? Select from a choice below.\n" +
                    "1) Show all items\n" +
                    "2) Add an item\n" +
                    "3) Remove an item\n" +
                    "4) Exit");

                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Console.Clear();
                        ShowAllItems();
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        break;
                    case "2":
                        Console.Clear();
                        AddItem();
                        break;
                    case "3":
                        Console.Clear();
                        DeleteItem();
                        break;
                    case "4":
                        continueToRun = false;
                        break;
                }
            }
        }

        public void ShowAllItems()
        {
            List<Menu> allItems = _repo.GetItems();

            foreach (Menu item in allItems)
            {
                string listOfIngredients = string.Join(",", item.Ingredients);
                Console.WriteLine($"Name: {item.Name}");
                Console.WriteLine($"Description: {item.Description}");
                Console.WriteLine($"Meal Number: {item.MealNumber}");
                Console.WriteLine($"Ingredients: {listOfIngredients}");
                Console.WriteLine($"Price:${item.Price}");
                Console.WriteLine("------------------");
            }
        }

        public void AddItem()
        {
            Menu newMenuItem = new Menu();
            Console.WriteLine("Please enter a name:");
            newMenuItem.Name = Console.ReadLine();

            Console.WriteLine($"Please enter a description for {newMenuItem.Name}");
            newMenuItem.Description = Console.ReadLine();

            Console.WriteLine($"Please enter the meal number for {newMenuItem}");
            newMenuItem.MealNumber = Int32.Parse(Console.ReadLine());

            Console.WriteLine($"Enter the ingredients for {newMenuItem.Name}");
            Console.WriteLine("Keep in mind: Items must be separated by commas and no spaces in the list are allowed.");
            string ingredientsList = Console.ReadLine();
            newMenuItem.Ingredients = ingredientsList.Split(',');

            Console.WriteLine($"Plese enter a price for {newMenuItem.Name}");
            newMenuItem.Price = decimal.Parse(Console.ReadLine());

            _repo.AddItem(newMenuItem);
            Console.WriteLine($"{newMenuItem.Name} was added.");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public void DeleteItem()
        {
            List<Menu> menuItems = _repo.GetItems();

            if (menuItems.Count == 0)
            {
                Console.WriteLine("There are no entries to remove.");
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }

            else
            {
                int count = 0;

                foreach (Menu item in menuItems)
                {
                    count++;
                    Console.WriteLine($"{count}) {item.Name}");
                }
                int targetID = int.Parse(Console.ReadLine());
                int actualID = targetID - 1;

                if (actualID >= 0 && actualID < menuItems.Count)
                {
                    Menu desiredItem = menuItems[actualID];
                    if (_repo.DeleteItem(desiredItem))
                    {
                        Console.WriteLine($"{desiredItem.Name} was successfully removed.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.Beep();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("500 Error: Please try again later.");
                        Console.ResetColor();
                        Console.ReadKey();
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Incorrect ID");
                    Console.ResetColor();
                    Console.ReadKey();
                }
            }
        }

        public void SeedContent()
        {
            string[] cheeseBurgerIngredients = { "Beef", "Lettuce", "Grilled Onions", "Ketchup", "Melted Cheese", " Seedless Bun" };
            Menu item1 = new Menu("Cheese Burger", "A delicious cheese burger with a variety of toppings that will melt in your mouth", 1, cheeseBurgerIngredients, 5.75m);

            string[] chickenSandwichIngredients = { "Chicken Patty", "Lettuce", "Ketchup", "Onions", "Buns" };
            Menu item2 = new Menu("Chicken Sandwich", "A savory chicken sandwich that will make your mouth water", 2, chickenSandwichIngredients, 5.25m);

            string[] cevapeIngredients = { "Flour", "Tortilla", "Beef", "Onions", "Sour Cream" };
            Menu item3 = new Menu("Cevape", "A Croatian Meaty Tortilla", 3, cevapeIngredients, 7.10m);

            string[] palacinkeIngredients = { "Crepe", "Nutella" };
            Menu item4 = new Menu("Palacinke", "A Croatian Crepe with Nutella", 4, palacinkeIngredients, 3.50m);

            _repo.AddItem(item1);
            _repo.AddItem(item2);
            _repo.AddItem(item3);
            _repo.AddItem(item4);
        }
    }
}