## Welcome To the Komodo Department. There are 3 console applications that encapsulate this project.
---
### The first console application is called The Komodo Cafe. The user is able to create a new menu item, delete a menu item and view all menu items. In short, this is a CRD Application (Create, Read, Delete). 
---
### The second console application is called The Komodo Claims Department. The user is able to handle a claim, enter a new claim and see all claims. This is another CRD Application (Create, Read, Delete). The user will be allowed to handle 3 different types of insurance claims: Home, Theft and Car Insurance. 
---
### The third console application is called Komodo Insurance. This application was a response to a request on fixing the badging system the company has been having issues with. Each Badge has access to certain doors. These badges and doors can be added, removed, updated and viewed by the user. This is a full CRUD Console Application. 
<br/>

### Each Console Application is broken into four sections. 

1. Console.
2. Repository.
3. UI
4. Unit Testing.

### 1. The Console section has a program class that runs the UI. 

### 2. The Repository section is a class library that holds 2 classes: Repository, and the main class (Menu, Badge, Claims.) The main class is where all the properties for the class is held. The Repository is where all the backend logic is held. 

### 3. The UI section is where the actual user interface is. This is what the user will see when they run the console application. 

### 4. The Unit Testing section is a Unit Test Library. This is where all the testing for the methods are held in. 

---

## What is CRUD?
<br/>

### CRUD (Create, Read/Retrieve, Update, Delete/Destroy) are the four basic functions of persistent storage. It provides four different functions to deal with user data or information. 

<br/>

## How does CRUD work?

<br/>

### 1. Let's say we want to retrieve our data. We must send a GET request to the server. The server will communicate back, locate your data, authorization and grant you access to your data. This allows you to view all your data. 
<br/>

### 2. If we want to create an account or create data, we must send a POST request to the server. The server will process this HTTP request, understand that there is no existing data or account, and create it for you and store it in the server. 

<br/>

### 3. If we want to update data, we must send a PUT request to the server. The server will recognize it is a PUT request and the specified data you are updating and it will make that change for you and update the database.

<br/>

### 4. Lastly, If we want to delete data, we must send a DELETE request to the server. Once, this HTTP request is sent, the server will look at the data you are trying to delete, match your credentials, grant you authorization, and finally process the action of deleting the specifiec data or item you wish to delete. After it has been deleted, the database updates and completely removes it from its existing database. 
