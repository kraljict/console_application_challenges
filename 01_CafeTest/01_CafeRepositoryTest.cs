﻿using System.Collections.Generic;
using _01_CafeRepository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _01_CafeTest
{
    [TestClass]
    public class CafeRepositoryTests
    {
        private Menu _item;
        private MenuRepository _repo;

        [TestInitialize]
        public void Arrange()
        {
            string[] palacinkeIngredients = { "Crepe", "Nutella" };
            _item = new Menu("Palacinke", "A Croatian Crepe with Nutella", 4, palacinkeIngredients, 3.50m);
            _repo = new MenuRepository();
            _repo.AddItem(_item);
        }

        [TestMethod]
        public void AddItem_ShouldGetTrue()
        {
            string[] palacinkeIngredients = { "Crepe", "Nutella" };
            Menu food = new Menu("Palacinke", "A Croatian Crepe with Nutella", 4, palacinkeIngredients, 3.50m);
            MenuRepository repository = new MenuRepository();
            bool addResult = repository.AddItem(food);
            Assert.IsTrue(addResult);
        }

        [TestMethod]
        public void DeleteItem_ShouldGetTrue()
        {
            Menu item = _item;
            bool deleteResult = _repo.DeleteItem(item);
            Assert.IsTrue(deleteResult);
        }

        [TestMethod]
        public void GetItems_ShouldReturnTrue()
        {
            List<Menu> menuItems = _repo.GetItems();
            bool listHasItems = menuItems.Contains(_item);
            Assert.IsTrue(listHasItems);
        }
    }
}
